// pages/index/detail.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},   // 用户信息
    hasUserInfo: false,  // 用户授权
    canIUse: wx.canIUse('button.open-type.getUserInfo'),   //  检测小程序版本兼容
    token: '',

    remark: '',
    wishes_index: '',
    big_wishes_image:'',
    wishes_images: [],
    wishes_images_count: [0, 0, 0, 0],
    total_money:0,
    total_char:0,

    cid:0,//页面来源
    tct_id:0,
    ownerImg: '', // 头像
    ownerName: '',// 名字
    xcxewm: '',
    demoKoulin: '',
  },
  onPullDownRefresh: function () {
    this.loadDetail()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var randomNum = this.randomNum(0, app.globalData.demoKoulin.length - 1),
      demoremark = app.globalData.demoKoulin[randomNum];
    this.setData({
      tct_id: options.tct_id,
      cid: options.cid,
      demoKoulin: demoremark
    })
  },
  randomNum: function (minNum, maxNum) {
    switch (arguments.length) {
      case 1:
        return parseInt(Math.random() * minNum + 1, 10);
        break;
      case 2:
        return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
        break;
      default:
        return 0;
        break;
    }
  },
  loadDetail: function(){
    var that = this;
    var data = {
      token: that.data.token,
      tct_id: that.data.tct_id
    }
    wx.request({
      url: app.setConfig.url + '/index.php/Api/taocaitou/detail',
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        wx.stopPullDownRefresh()
        console.log(res)
        if (res.data.code == 20000) {
          var content = res.data.data.tct_info.content
          var index = app.globalData.wishes.indexOf(content)
          var count = res.data.data.tct_info.num_arr

          var indexstr = '';
          if (index == 0) {
            indexstr = "0";
          } else if (index == 1) {
            indexstr = "1";
          } else {
            indexstr = index.toString();
          };
          const baseSource = app.globalData.imgBaseUrl
          that.setData({
            total_money: res.data.data.tct_info.amount,
            total_char: res.data.data.tct_info.num,
            wishes_images_count: res.data.data.tct_info.num_arr,
            remark: res.data.data.tct_info.remark,
            wishes_index: index,
            big_wishes_image: baseSource + indexstr + '.png',
            wishes_images: [
              baseSource + indexstr + (count[0] == 0 ? '-1' : '-1s') + '.png',
              baseSource + indexstr + (count[1] == 0 ? '-2' : '-2s') + '.png',
              baseSource + indexstr + (count[2] == 0 ? '-3' : '-3s') + '.png',
              baseSource + indexstr + (count[3] == 0 ? '-4' : '-4s') + '.png'
            ],
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            mask: true,
            duration: 1500
          })
        }
      },
      fail: function (res) {
        wx.stopPullDownRefresh()
        console.log(res)
      }
    })
  },
  //转发
  onShareAppMessage: function (res) {
    var that = this

    var index = that.data.wishes_index
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };

    var id = that.data.tct_id
    var title = that.data.remark
    return {
      title: title,
      path: 'pages/give/give?tct_id=' + id,
      imageUrl: app.globalData.imgBaseUrl + 'sharefriend-' + indexstr +'.png'
    }
  },
  
  //获取登录信息
  onShow: function () {
    var that = this
    if (app.globalData.userInfo && app.globalData.token){
      var info = app.globalData.userInfo,
        tok = app.globalData.token;
      that.setData({
        userInfo: info,
        hasUserInfo: true,
        token: tok,
        ownerName: info.nickName,
        ownerImg: info.avatarUrl
      })
      that.loadDetail()
    }else{
      that.loop()
    }
  },
  //生成朋友圈分享图
  wxzone: function () {
    wx.showLoading({
      title: '图片生成中...',
    })
    var that = this
    //二维码
    var postUrl = app.setConfig.url + '/index.php?g=Api&m=ToCode&a=get_code',
      postData = {
        token: app.globalData.token,
        tct_id: that.data.tct_id,
        tit: that.data.ownerName,
        con: '',
        page: 'pages/give/give'
      };
    app.postLogin(postUrl, postData, this.setCode);
  },
  setCode: function (res) {
    if (res.data.code === 20000) {
      var datas = res.data;
      this.setData({
        xcxewm: app.setConfig.url + '/' + datas.data,
      })
      var fximg = this.data.xcxewm;
      wx.previewImage({
        current: fximg, //当前显示图片的http链接
        urls: [fximg]//需要预览的图片http链接列表
      })
    }
  },
  loop: function () {
    var info = app.globalData.userInfo,
      tok = app.globalData.token;
    if (info && !this.data.hasUserInfo) {
      this.setData({
        userInfo: info,
        hasUserInfo: true,
        ownerName: info.nickName,
        ownerImg: info.avatarUrl
      })
    }
    if (!tok) {
      var that = this
      setTimeout(function () { that.loop(); }, 10)
    } else {
      this.setData({
        token: tok
      })
      if (app.globalData.act_message === '') { } else {
        this.setData({
          text: app.globalData.act_message
        })
        this.scrollTipMessage()
      }
      this.loadDetail()
    }
  },
  anotherone: function () {
    if (this.data.cid === '998'){
      wx.navigateBack({
        delta: 1,
      })
    }else{
      wx.reLaunch({
        url: '../index/index',
      })
    }
  },
  gowithdraw: function () {
    wx.navigateTo({
      url: '../record/record',
    })
  },
  gotodetail: function (){
    wx.navigateTo({
      url: '../recordDetails/recordDetails?tct_id=' + this.data.tct_id,
    })
  },
})