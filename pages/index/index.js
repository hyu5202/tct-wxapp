//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    userInfo: {},   // 用户信息
    hasUserInfo: false,  // 用户授权
    show_view :false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),   //  检测小程序版本兼容
    token: '',
    demoKoulin: '',

    animationData: {},
    wish_index:0,
    wishes_images:[],
    big_wishes_image: app.globalData.imgBaseUrl + '1.png',
    
    text: '',
    marqueePace: 1,//滚动速度
    marqueeDistance: 0,//初始滚动距离
    marquee2copy_status: false,
    marquee2_margin: 60,
    size: 14,
    orientation: 'left',//滚动方向
    interval: 30 // 时间间隔
  },
  onLoad:function(){
    var self = this;
    if (app.globalData.userInfo && app.globalData.token) {
      var info = app.globalData.userInfo,
        tok = app.globalData.token,
        randomNum = self.randomNum(0, app.globalData.demoKoulin.length - 1),
        demoremark = app.globalData.demoKoulin[randomNum];
      self.setData({
        userInfo: info,
        hasUserInfo: true,
        token: tok,
        show_view:true,
        demoKoulin:demoremark
      })
      if (app.globalData.act_message === ''){}else{
        self.setData({
          text: app.globalData.act_message
        })
        self.scrollTipMessage()
      }
      if (app.globalData.tip_message === '') { } else {
        wx.showModal({
          title: '提示',
          content: app.globalData.tip_message,
          showCancel: false
        })
      }
      self.randomWishes()
    } else {
      self.loop()
    }

  },
  randomNum: function (minNum, maxNum) {
    switch (arguments.length) {
      case 1:
        return parseInt(Math.random() * minNum + 1, 10);
        break;
      case 2:
        return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
        break;
      default:
        return 0;
        break;
    }
  },
  randomWishes: function (){
    var self = this;
    const randomWish = self.randomNum(0, app.globalData.wishes.length - 1);
    const baseSource = app.globalData.imgBaseUrl
    var index = '';
    if (randomWish == 0) {
      index = "0";
    } else if (randomWish == 1) {
      index = "1";
    } else {
      index = randomWish.toString();
    };
    self.setData({
      wish_index: randomWish,
      wishes_images: [
        baseSource + index + '-1s.png',
        baseSource + index + '-2s.png',
        baseSource + index + '-3s.png',
        baseSource + index + '-4s.png'
      ],
    })
  },
  
  formSubmit:function(e){
    var that = this;
    wx.showLoading({
      title: '',
    })
    wx.showLoading({
      title: '',
      mask: true
    })
    var data = {
      token: that.data.token,
      content: app.globalData.wishes[that.data.wish_index],
      remark: that.data.demoKoulin,
      form_id: e.detail.formId,
    }
    wx.request({
      url: app.setConfig.url + '/index.php/Api/taocaitou/create',
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.code == 20000){
          var tct_id = res.data.data.tct_id
          wx.hideLoading()
          wx.navigateTo({
            url: '../index/detail?tct_id=' + tct_id + '&cid=' + 998,
          })
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            mask: true,
            duration: 1500
          })
        }
      }
    })
  },
  changeoneBtClick:function(){
    var that = this
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear',
    })
    that.animation = animation;
    animation.rotateY(90).step()
    that.setData({
      animationData: animation.export()
    })

    setTimeout(function () {
      var nanimation = wx.createAnimation({
        duration: 500,
        timingFunction: 'linear',
      })
      nanimation.rotateY(0).step()
      that.randomWishes()
      that.setData({
        animationData: nanimation.export(),
      })
    }.bind(that), 500)
  },
  loop: function () {
    var that = this
    var info = app.globalData.userInfo,
        tok = app.globalData.token;
    if (info && !that.data.hasUserInfo){
      that.setData({
        userInfo: info,
        hasUserInfo: true
      })
    }
    if (!tok) {
      setTimeout(function () { that.loop(); }, 10)
    } else {
      if (app.globalData.news_key) {
        wx.setClipboardData({
          data: app.globalData.news_key,
          success: function (res) {
          }
        })
      }
      wx.hideLoading()
      var randomNum = that.randomNum(0, app.globalData.demoKoulin.length - 1);
      var demoremark = app.globalData.demoKoulin[randomNum];
      that.setData({
        token: tok,
        demoKoulin: demoremark,
        show_view: true
      })
      if (app.globalData.act_message === '') { }else{
        that.setData({
          text: app.globalData.act_message
        })
        that.scrollTipMessage()
      }
      if (app.globalData.tip_message === '') { } else {
        wx.showModal({
          title: '提示',
          content: app.globalData.tip_message,
          showCancel: false
        })
      }
      that.randomWishes()
    }
  },
  gorecords: function(){
    wx.navigateTo({
      url: '../record/record',
    })
  },
  gowithdraw:function(){
    wx.navigateTo({
      url: '../balance/balance',
    })
  },
  goqa: function(){
    wx.navigateTo({
      url: '../help/help',
    })
  },
  gorank: function () {
    wx.navigateTo({
      url: '../rank/list',
    })
  },
  //转发
  onShareAppMessage: function (res) {
    return {
      title: '集字讨彩头，瓜分现金大奖',
      path: 'pages/index/index',
      imageUrl: ""
    }
  },
  scrollTipMessage:function(){
    var vm = this;
    var length = vm.data.text.length * vm.data.size;//文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth;// 屏幕宽度
    if (length > windowWidth){
      vm.setData({
        length: length,
        windowWidth: windowWidth,
        marquee2_margin: length < windowWidth ? windowWidth - length : vm.data.marquee2_margin//当文字长度小于屏幕长度时，需要增加补白
      });
      setTimeout(function () {
        vm.run();// 第一个字消失后立即从右边出现
      }.bind(vm), 1000)
    }
  },
  run: function () {
    var vm = this;
    var interval = setInterval(function () {
      if (-vm.data.marqueeDistance < vm.data.length) {
        // 如果文字滚动到出现marquee2_margin=30px的白边，就接着显示
        vm.setData({
          marqueeDistance: vm.data.marqueeDistance - vm.data.marqueePace,
          marquee2copy_status: vm.data.length + vm.data.marqueeDistance <= vm.data.windowWidth + vm.data.marquee2_margin,
        });
      } else {
        if (-vm.data.marqueeDistance >= vm.data.marquee2_margin) { // 当第二条文字滚动到最左边时
          vm.setData({
            marqueeDistance: vm.data.marquee2_margin // 直接重新滚动
          });
          clearInterval(interval);
          vm.run();
        } else {
          clearInterval(interval);
          vm.setData({
            marqueeDistance: -vm.data.windowWidth
          });
          vm.run();
        }
      }
    }, vm.data.interval);
  }
})
