//balance.js
//获取应用实例
const app = getApp()

Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    token:'',
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    amount:'0.00',   // 账户余额
    sum:'',          // 提现金额

    minsum: '',   //最小提现金额
    maxnum: '',   // 最大提现次数
    num: '',      // 可以提现次数
    advpt: {},    //平台广告

    fee_percent:0,
    min_arrive:1, //最小到账金额
    service_fee:0.00, //手续费
    arriveind_fee:0.00,//实际到账金额

    text: '',
    marqueePace: 1,//滚动速度
    marqueeDistance: 0,//初始滚动距离
    marquee2copy_status: false,
    marquee2_margin: 60,
    size: 14,
    orientation: 'left',//滚动方向
    interval: 30 // 时间间隔
  },
  //广告跳转
  gotoPlatformAdv: function () {
    if (this.data.advpt.slide_des == '小程序跳转') {
      var appid = this.data.advpt.slide_name;
      var path = this.data.advpt.slide_url;
      wx.navigateToMiniProgram({
        appId: appid,
        path: path,
        success(res) {
        }
      });
    } else if (this.data.advpt.slide_des == '页面跳转') {
      wx.navigateTo({
        url: "/pages/webview/webview?url=" + this.data.advpt.slide_url
      })
    } 
  },
  //全部提现
  entirely: function(e){ 
    var amount = parseFloat(this.data.amount);
    if (amount == 0){
      wx.showToast({
        title: '没有余额',
        icon: 'loading',
        duration: 1000
      })
    }else{
      this.setData({
        sum: this.data.amount,
        service_fee: (this.data.amount * this.data.fee_percent).toFixed(2),
        arriveind_fee: (this.data.amount - this.data.amount * this.data.fee_percent).toFixed(2)
      }) 
    }
  },
  // 提现输入框金额判断
  bindKeyInput:function(e){
    var inp = Math.round(e.detail.value*100)/100;
    var max = parseFloat(this.data.amount);
    if(max == 0&&inp != ''){
      wx.showToast({
        title: '没有余额',
        icon: 'loading',
        duration: 1000
      })
      return false;
    }
    inp = inp > max ? max : inp;
    var minsum = parseFloat(this.data.minsum);
    if (inp > 0){
      inp = inp < minsum ? minsum.toFixed(2) : inp.toFixed(2);
      inp = max < minsum ? max.toFixed(2) : inp;
    }else{
      inp = '';
    }
    this.setData({
      sum: inp,
      service_fee: (inp * this.data.fee_percent).toFixed(2),
      arriveind_fee: (inp - inp * this.data.fee_percent).toFixed(2)
    })
  },
  // 表单提交
  formSubmit: function (e) {
    var that = this;
    
    //保存formid
    var postData = {
      form_id: e.detail.formId,
      token: app.globalData.token
    };
    wx.request({
      url: app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=saveFormid',
      data: postData,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
      }
    })
    //提现
    
    var val = Math.round(e.detail.value.txje * 100) / 100,
        max = parseFloat(that.data.amount),
        minsum = parseFloat(that.data.minsum),
        maxnum = parseFloat(that.data.maxnum),
        num = parseFloat(that.data.num);

    if (that.data.amount == 0) {
      wx.showModal({
        title: '提示',
        content: '没有余额',
        showCancel: false
      })
      return false;
    }

    val = val > max ? max : val;
    if (val > 0) {
      val = val < minsum ? minsum : val;
      val = max < minsum ? max : val;
    } else {
      val = '';
    }
    if (val >= minsum) {
      wx.showModal({
        title: '提示',
        content: '确定提现' + val.toFixed(2) + '元?',
        success: function (res) {
          if(res.confirm){
            var postUrl = app.setConfig.url + '/index.php?g=Api&m=Withdrawals&a=cash';
            var postData = {
              'amount': val.toFixed(2),
              'token': that.data.token,
            }
            app.postLogin(postUrl, postData, function (res) {
              if (res.data.code == 20000) {
                wx.showToast({
                  title: '提现成功',
                  icon: 'success',
                  duration: 1500
                })
                that.setData({
                  amount: (max - val).toFixed(2),
                  sum: ''
                })
              }
            });
          }
        }
      })
    } else if (val > 0){
      wx.showModal({
        title: '提示',
        content: '提现最低' + minsum + '元',
        showCancel: false
      })
      return false;
    }
    if (that.data.arriveind_fee < 1) {
      wx.showModal({
        title: '提示',
        content: '到账金额不能小于1元',
        showCancel: false
      })
      return false;
    }
  },
  onShow: function () {
    if (app.globalData.news_key) {
      wx.setClipboardData({
        data: app.globalData.news_key,
        success: function (res) {
        }
      })
    }
    var tok = app.globalData.token;
    var info = app.globalData.userInfo;
    this.setData({
      userInfo: info,
      token: tok,
      hasUserInfo: true,
      advpt: app.globalData.balanceAdv
    })
    if (app.globalData.act_message === '') { } else {
      this.setData({
        text: app.globalData.act_message
      })
      this.scrollTipMessage()
    }
    var postUrl = app.setConfig.url + '/index.php?g=Api&m=Withdrawals&a=amountAndAdv',
      postData = {
        token: tok
      };
    app.postLogin(postUrl, postData, this.initial);  
  },
  initial:function(res){
    if (res.data.code == 20000) {
      var data = res.data;
      this.setData({
        amount: data.amount,
        minsum: data.min_withdrawals, 
        maxnum: data.max_withdrawal_time,
        num: data.withdrawal_time,
        fee_percent: data.withdrawal_ratio,
      })
    }
  },
  scrollTipMessage: function () {
    var vm = this;
    var length = vm.data.text.length * vm.data.size;//文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth;// 屏幕宽度
    if (length > windowWidth) {
      vm.setData({
        length: length,
        windowWidth: windowWidth,
        marquee2_margin: length < windowWidth ? windowWidth - length : vm.data.marquee2_margin//当文字长度小于屏幕长度时，需要增加补白
      });
      setTimeout(function () {
        vm.run();// 第一个字消失后立即从右边出现
      }.bind(vm), 1000)
    }
  },
  run: function () {
    var vm = this;
    var interval = setInterval(function () {
      if (-vm.data.marqueeDistance < vm.data.length) {
        // 如果文字滚动到出现marquee2_margin=30px的白边，就接着显示
        vm.setData({
          marqueeDistance: vm.data.marqueeDistance - vm.data.marqueePace,
          marquee2copy_status: vm.data.length + vm.data.marqueeDistance <= vm.data.windowWidth + vm.data.marquee2_margin,
        });
      } else {
        if (-vm.data.marqueeDistance >= vm.data.marquee2_margin) { // 当第二条文字滚动到最左边时
          vm.setData({
            marqueeDistance: vm.data.marquee2_margin // 直接重新滚动
          });
          clearInterval(interval);
          vm.run();
        } else {
          clearInterval(interval);
          vm.setData({
            marqueeDistance: -vm.data.windowWidth
          });
          vm.run();
        }
      }
    }, vm.data.interval);
  }
})
