//help.js
const app = getApp()

Page({
  data: {
    lists: [],
    control:-1,
    version:'1.0.0',
    platformID: 1, //平台广告id
    platformAdvImg: null, //平台广告图片
    gotoPlatformAdvdetial: "/pages/record/record/ptadvID=", //平台广告链接
    advpt: null   //平台广告
  },
  onshow: function(e){
    if (this.data.control === e.target.dataset.id){
      this.setData({
        control: -1,
      })
    }else{
      this.setData({
        control: e.target.dataset.id,
      })
    }
  },
  //拨打电话
  tel: function () {
    wx.makePhoneCall({
      phoneNumber: '13242857521'
    })
  },
  one: function(){},
  onLoad: function () {
    this.setData({
      lists:app.globalData.helpList,
      advpt: app.globalData.helpAdv
    })
    var res = wx.getSystemInfoSync();
  },
  //广告跳转
  gotoPlatformAdv:function(){
    if (this.data.advpt.slide_des == '小程序跳转') {
      var appid = this.data.advpt.slide_name;
      var path = this.data.advpt.slide_url;
      wx.navigateToMiniProgram({
        appId: appid,
        path: path,
        success(res) {
        }
      }); 
    } else if (this.data.advpt.slide_des == '页面跳转'){
      wx.navigateTo({
        url: "/pages/webview/webview?url=" + this.data.advpt.slide_url
      })
    }
      
  },
  //获取formid
  formSubmit: function (e) {
    var postData = {
      form_id: e.detail.formId,
      token: app.globalData.token
    };
    wx.request({
      url: app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=saveFormid',
      data: postData,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
      }
    })
  },
})
