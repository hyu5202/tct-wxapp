//recordDetails.js
//获取应用实例
const app = getApp()

Page({
  data: {
    wishes_index: '',
    wishes_images: [],
    wishes_images_count: [0, 0, 0, 0],
    total_money: 0,
    total_char: 0,
    tct_id: 0,
    friends:[],
    remark:'',

    ownerImg: '', // 头像
    ownerName: '',// 名字
    xcxewm: '',
  },
  onLoad: function (options) {
    this.setData({
      tct_id: options.tct_id
    })
    this.loadDetail()
  },
  loadDetail: function () {
    var that = this;
    wx.showLoading({
      title: '',
      mask: true
    })
    var data = {
      token: app.globalData.token,
      tct_id: that.data.tct_id
    }
    wx.request({
      url: app.setConfig.url + '/index.php/Api/taocaitou/detailTwo',
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.code == 20000) {
          wx.hideLoading()
          var content = res.data.data.tct_info.content
          var index = app.globalData.wishes.indexOf(content)
          var count = res.data.data.tct_info.num_arr
          var indexstr = '';
          if (index == 0) {
            indexstr = "0";
          } else if (index == 1) {
            indexstr = "1";
          } else {
            indexstr = index.toString();
          };
          const baseSource = app.globalData.imgBaseUrl
          that.setData({
            remark: res.data.data.tct_info.remark,
            friends: res.data.data.list,
            total_money: res.data.data.tct_info.amount,
            total_char: res.data.data.tct_info.num,
            wishes_images_count: res.data.data.tct_info.num_arr,
            wishes_index: index,
            wishes_images: [
              baseSource + indexstr + (count[0] == 0 ? '-1' : '-1s') + '.png',
              baseSource + indexstr + (count[1] == 0 ? '-2' : '-2s') + '.png',
              baseSource + indexstr + (count[2] == 0 ? '-3' : '-3s') + '.png',
              baseSource + indexstr + (count[3] == 0 ? '-4' : '-4s') + '.png'
            ],
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            mask: true,
            duration: 1500
          })
        }
      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  //转发
  onShareAppMessage: function (res) {
    var that = this
    var index = that.data.wishes_index
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };

    var id = that.data.tct_id
    var remark = that.data.remark
    return {
      title: remark,
      path: 'pages/give/give?tct_id=' + id,
      imageUrl: app.globalData.imgBaseUrl + 'sharefriend-' + indexstr + '.png'
    }
  },
  //生成朋友圈分享图
  wxzone: function () {
    wx.showLoading({
      title: '图片生成中...',
    })
    var that = this
    var info = app.globalData.userInfo
    that.setData({
      ownerName: info.nickName,
      ownerImg: info.avatarUrl
    })
    //二维码
    var postUrl = app.setConfig.url + '/index.php?g=Api&m=ToCode&a=get_code',
      postData = {
        token: app.globalData.token,
        tct_id: that.data.tct_id,
        tit: that.data.ownerName,
        con: '',
        page: 'pages/give/give'
      };
    app.postLogin(postUrl, postData, this.setCode);  
  },
  setCode: function (res) {
    var that = this
    wx.hideLoading()
    if (res.data.code === 20000) {
      var datas = res.data;
      that.setData({
        xcxewm: app.setConfig.url + '/' + datas.data,
      })
      var fximg = this.data.xcxewm;
      wx.previewImage({
        current: fximg, //当前显示图片的http链接
        urls: [fximg]//需要预览的图片http链接列表
      })
    }
  },
})
