//record.js
//获取应用实例
const app = getApp();
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),

    pagel: 0,      //  发出的 页数
    pager: 0,      //  收到的 页数
    lower0: true,      //  触底加载控制器
    lower1: true,      //  触底加载控制器
    indicatorDots: false,   // 滑块参数
    duration: 500,      // 切换时间 
    current:0,     // 切换指数
    platformAdvImg: null, //平台广告图片
    wishPath: '',
    //我参与的
    sendAmount:0,
    sendNum: 0,
    ranklist: [
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": ""},
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": "" },
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": "" },
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": "" },
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": "" },
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": "" },
      { "rand": "", "user_id": "", "nick_name": "", "head_img": "", "total_num": "", "total_amount": "" }
    ],
    rank_amount: ''
  },
  // 触底加载数据
  lower:function(e){
    var that = this;
    this.loaddatar();

  },
  onLoad: function () {
    var info = app.globalData.userInfo,
        tok = app.globalData.token;
    this.setData({
      userInfo: info,
      token: tok,
      hasUserInfo: true,
      wishPath: app.globalData.imgBaseUrl,
      rank_amount: app.globalData.rank_amount
    })
    
    this.loaddatar();
    if (app.globalData.news_key){
      wx.setClipboardData({
        data: app.globalData.news_key,
        success: function (res) {
        }
      })
    }
  },
  // 收到的数据 加载
  loaddatar:function(){
    wx.showLoading({
      title: '加载中•••'
    })
    var tok = this.data.token;
    var postUrl = app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=rankList',
        postData = {
          token: tok
        };
    app.postLogin(postUrl, postData, this.reciveList); 
  },
  //收到的包
  reciveList:function(res){
    if (res.data.code == 20000){
      var datas = res.data;
      wx.hideLoading();
      this.setData({
        ranklist: datas.data.list
      })
    }
  }
  
})
