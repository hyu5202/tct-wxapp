// pages/give/give.js
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},   // 用户信息
    hasUserInfo: false,  // 用户授权
    canIUse: wx.canIUse('button.open-type.getUserInfo'),   //  检测小程序版本兼容
    token: '',
    content:'',
    remark: '',
    wishes_index: '',
    wishes_images: [],
    wishes_images_count: [0, 0, 0, 0],
    tct_id: 0,
    friends_wishes:[],

    head_img:'',
    nick_name:'',
    ci_wish_img: '',
    ci_wish_char:'',
    money:'',
    feed_back:'',
    seleted_index:0,
  },
  onLoad: function (options) {
    var scene = decodeURIComponent(options.scene);
    if (scene > 0) {
      this.setData({
        tct_id: scene
      })
    } else {
      this.setData({
        tct_id: options.tct_id
      })
    }
    if (app.globalData.news_key) {
      wx.setClipboardData({
        data: app.globalData.news_key,
        success: function (res) {
        }
      })
    } 
  },
  onPullDownRefresh: function () {
    this.loadDetail()
  },
  loadDetail: function () {
    var that = this;
    var data = {
      token: that.data.token,
      tct_id: that.data.tct_id
    }
    wx.request({
      url: app.setConfig.url + '/index.php/Api/taocaitou/detail',
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        wx.stopPullDownRefresh()
        if (res.data.code == 20000) {
          var is_self = res.data.data.tct_info.is_self
          if (is_self == 1){
            wx.redirectTo({
              url: '../index/detail?tct_id=' + that.data.tct_id,
            })
          }else{
            var rcontent = res.data.data.tct_info.content
            var index = app.globalData.wishes.indexOf(rcontent)
            var count = res.data.data.tct_info.num_arr
            var indexstr = '';
            if (index == 0) {
              indexstr = "0";
            } else if (index == 1) {
              indexstr = "1";
            } else {
              indexstr = index.toString();
            };

            var wish = app.globalData.wishes[index]
            var char = wish.split(",")

            var baseSource = app.globalData.imgBaseUrl
            that.setData({
              seleted_index:0,
              content: rcontent,
              head_img: res.data.data.tct_info.head_img,
              nick_name: res.data.data.tct_info.nick_name,
              wishes_images_count: res.data.data.tct_info.num_arr,
              remark: res.data.data.tct_info.remark,
              wishes_index: index,
              ci_wish_char: char[0],
              ci_wish_img: baseSource + indexstr + '-1a.png',
              money: app.globalData.moneys[that.randomNum(0, app.globalData.moneys.length - 1)],
              feed_back:app.globalData.feed_backs[that.randomNum(0, app.globalData.feed_backs.length - 1)],
              wishes_images: [
                baseSource + indexstr + '-1s' + '.png',
                baseSource + indexstr + (count[1] == 0 ? '-2' : '-2s') + '.png',
                baseSource + indexstr + (count[2] == 0 ? '-3' : '-3s') + '.png',
                baseSource + indexstr + (count[3] == 0 ? '-4' : '-4s') + '.png'
              ],
              friends_wishes: res.data.data.list,
            })
          }
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            mask: true,
            duration: 1500
          })
        }
      },
      fail: function (res) {
        wx.stopPullDownRefresh()
        console.log(res)
      }
    })
  },
  //获取登录信息
  onShow: function () {
    if (app.globalData.userInfo && app.globalData.token) {
      var info = app.globalData.userInfo,
        tok = app.globalData.token;
      this.setData({
        userInfo: info,
        hasUserInfo: true,
        token: tok
      })
      this.loadDetail()
    } else {
      this.loop()
    }
  },
  loop: function () {
    var info = app.globalData.userInfo,
      tok = app.globalData.token;
    if (info && !this.data.hasUserInfo) {
      this.setData({
        userInfo: info,
        hasUserInfo: true
      })
    }
    if (!tok) {
      var that = this
      setTimeout(function () { that.loop(); }, 10)
    } else {
      this.setData({
        token: tok
      })
      this.loadDetail()
    }
  },
  metoo:function(){
    wx.redirectTo({
      url: '../index/index',
    })
  },
  //转发
  onShareAppMessage: function (res) {
    var that = this

    var index = that.data.wishes_index
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };

    var id = that.data.tct_id
    var remark = that.data.remark
    return {
      title: remark,
      path: 'pages/give/give?tct_id=' + id,
      imageUrl: app.globalData.imgBaseUrl + 'sharefriend-' + indexstr +'.png'
    }
  },
  randomNum: function (minNum, maxNum) {
    switch (arguments.length) {
      case 1:
        return parseInt(Math.random() * minNum + 1, 10);
        break;
      case 2:
        return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
        break;
      default:
        return 0;
        break;
    }
  },
  redbaoitem0tap: function () {
    var that = this
    var baseSource = app.globalData.imgBaseUrl
    var rcontent = that.data.content
    var index = app.globalData.wishes.indexOf(rcontent)
    var count = that.data.wishes_images_count
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };
    var wish = app.globalData.wishes[index]
    var char = wish.split(",")

    that.setData({
      seleted_index:0,
      ci_wish_char: char[0],
      ci_wish_img: baseSource + indexstr + '-1a.png',
      money: app.globalData.moneys[that.randomNum(0, app.globalData.moneys.length - 1)],
      feed_back: app.globalData.feed_backs[that.randomNum(0, app.globalData.feed_backs.length - 1)],
      wishes_images: [
        baseSource + indexstr + '-1s' + '.png',
        baseSource + indexstr + (count[1] == 0 ? '-2' : '-2s') + '.png',
        baseSource + indexstr + (count[2] == 0 ? '-3' : '-3s') + '.png',
        baseSource + indexstr + (count[3] == 0 ? '-4' : '-4s') + '.png'
      ],
    })
  },
  redbaoitem1tap: function () {
    var that = this
    var baseSource = app.globalData.imgBaseUrl
    var rcontent = that.data.content
    var index = app.globalData.wishes.indexOf(rcontent)
    var count = that.data.wishes_images_count
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };
    var wish = app.globalData.wishes[index]
    var char = wish.split(",")

    that.setData({
      seleted_index: 1,
      ci_wish_char: char[1],
      ci_wish_img: baseSource + indexstr + '-2a.png',
      money: app.globalData.moneys[that.randomNum(0, app.globalData.moneys.length - 1)],
      feed_back: app.globalData.feed_backs[that.randomNum(0, app.globalData.feed_backs.length - 1)],
      wishes_images: [
        baseSource + indexstr + (count[0] == 0 ? '-1' : '-1s') + '.png',
        baseSource + indexstr + '-2s' + '.png',
        baseSource + indexstr + (count[2] == 0 ? '-3' : '-3s') + '.png',
        baseSource + indexstr + (count[3] == 0 ? '-4' : '-4s') + '.png'
      ],
    })
  },
  redbaoitem2tap: function () {
    var that = this
    var baseSource = app.globalData.imgBaseUrl
    var rcontent = that.data.content
    var index = app.globalData.wishes.indexOf(rcontent)
    var count = that.data.wishes_images_count
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };
    var wish = app.globalData.wishes[index]
    var char = wish.split(",")

    that.setData({
      seleted_index: 2,
      ci_wish_char: char[2],
      ci_wish_img: baseSource + indexstr + '-3a.png',
      money: app.globalData.moneys[that.randomNum(0, app.globalData.moneys.length - 1)],
      feed_back: app.globalData.feed_backs[that.randomNum(0, app.globalData.feed_backs.length - 1)],
      wishes_images: [
        baseSource + indexstr + (count[0] == 0 ? '-1' : '-1s') + '.png',
        baseSource + indexstr + (count[1] == 0 ? '-2' : '-2s') + '.png',
        baseSource + indexstr + '-3s' + '.png',
        baseSource + indexstr + (count[3] == 0 ? '-4' : '-4s') + '.png'
      ],
    })
  },
  redbaoitem3tap: function () {
    var that = this
    var baseSource = app.globalData.imgBaseUrl
    var rcontent = that.data.content
    var index = app.globalData.wishes.indexOf(rcontent)
    var count = that.data.wishes_images_count
    var indexstr = '';
    if (index == 0) {
      indexstr = "0";
    } else if (index == 1) {
      indexstr = "1";
    } else {
      indexstr = index.toString();
    };
    var wish = app.globalData.wishes[index]
    var char = wish.split(",")

    that.setData({
      seleted_index: 3,
      ci_wish_char: char[3],
      ci_wish_img: baseSource + indexstr + '-4a.png',
      money: app.globalData.moneys[that.randomNum(0, app.globalData.moneys.length - 1)],
      feed_back: app.globalData.feed_backs[that.randomNum(0, app.globalData.feed_backs.length - 1)],
      wishes_images: [
        baseSource + indexstr + (count[0] == 0 ? '-1' : '-1s') + '.png',
        baseSource + indexstr + (count[1] == 0 ? '-2' : '-2s') + '.png',
        baseSource + indexstr + (count[2] == 0 ? '-3' : '-3s') + '.png',
        baseSource + indexstr + '-4s' + '.png'
      ],
    })
  },
  formSubmit: function (e) {
    var that = this
    var datas = {
      token : that.data.token,
      form_id : e.detail.formId,
      amount : that.data.money,
      quest : that.data.ci_wish_char,
      tct_id: that.data.tct_id,
    }
    var postUrl = app.setConfig.url + '/index.php/Api/taocaitou/saveEnve';
    app.postLogin(postUrl, datas, that.saveEnve);
  },
  // 提交
  saveEnve: function (res) {
    var that = this;
    if(res.data.code === 20000){
      var payInfo = res.data.data
      var pid = payInfo.pid
      var tct_id = payInfo.tct_id
      if (payInfo.pay_type == 2) {
  
        wx.showToast({
          title: '支付成功',
          mask: true,
          icon: 'success',
          duration: 1000
        })
        setTimeout(function () {
          that.loadDetail()
        }, 1000)
      }else{
        wx.requestPayment({
          'timeStamp': payInfo.timeStamp,
          'nonceStr': payInfo.nonceStr,
          'package': payInfo.package,
          'signType': 'MD5',
          'paySign': payInfo.paySign,
          'success': function (res) {
            wx.showToast({
              title: '支付成功',
              icon: 'success',
              duration: 1000
            })
            var postUrlTZ = app.setConfig.url + '/index.php?g=Api&m=taocaitou&a=sendCreateEnveNotify',
              postDataTZ = {
                token: app.globalData.token,
                prepay_id: payInfo.prepay_id,
                quest: payInfo.quest,
                tct_id: payInfo.tct_id,
                pid: payInfo.pid
              };
            app.postLogin(postUrlTZ, postDataTZ);
            setTimeout(function () {
              that.loadDetail()
            }, 1000)
          },
          'fail': function (res) {
            // 支付失败
            wx.showToast({
              title: '支付失败',
              icon: 'loading',
              mask: true,
              duration: 1000
            })
            that.setData({
              
            })
            //释放冻结金额
            var postUrl = app.setConfig.url + '/index.php?g=User&m=Consumer&a=rurnFrozenAmount',
              postData = {
                token: app.globalData.token
              };
            app.postLogin(postUrl, postData);
          }
        })
      }
    }
  }
})