//record.js
//获取应用实例
const app = getApp();
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),

    pagel: 0,      //  发出的 页数
    pager: 0,      //  收到的 页数
    lower0: true,      //  触底加载控制器
    lower1: true,      //  触底加载控制器
    indicatorDots: false,   // 滑块参数
    duration: 500,      // 切换时间 
    current:0,     // 切换指数
    platformAdvImg: null, //平台广告图片
    wishPath: '',
    //我发起的
    receiveAmount: 0,
    receiveNum: 0,
    sendHide:false,
    sendlist: [],
    //我参与的
    sendAmount:0,
    sendNum: 0,
    enevlist: [],
    enevHide:true,
    tct_id:0,
    form_id:''
  },
  //事件处理函数
  bindchange: function (e) {
    var ii = e.detail.current;
    this.setData({
      current: ii
    }) 
    if (ii == 1) {
      this.setData({
        sendHide: true,
        enevHide: false
      })
    } else if (ii == 0) {
      this.setData({
        sendHide: false,
        enevHide: true
      })
    }     
    if (ii == 1 && this.data.pager == 0){
      this.loaddatar(); 
    }else if(ii == 0 && this.data.pagel == 0){
      this.loaddatal(); 
    }   
  },
  // 触底加载数据
  lower:function(e){
    var that = this;
    var cd = e.target.dataset.current;
    if (this.data.lower0 && cd == 0){
      this.setData({
        lower0: false
      })
      this.loaddatal(); 
    }
    if (this.data.lower1 && cd == 1) {
      this.setData({
        lower1: false
      })
      this.loaddatar();
    }

  },
  swichNav: function (e) {
    var that = this;
    if (this.data.current == e.currentTarget.dataset.current) {
      return false;
    } else {
      that.setData({
        current: e.currentTarget.dataset.current
      })
    }
  },
  onLoad: function () {
    var info = app.globalData.userInfo,
        tok = app.globalData.token;
    this.setData({
      userInfo: info,
      token: tok,
      hasUserInfo: true,
      wishPath: app.globalData.imgBaseUrl,
    })
    
    this.loaddatal();
    
  },

  // 发出的数据 加载
  loaddatal:function(){
    wx.showLoading({
      title: '加载中•••'
    })
    var tok = this.data.token;
    var postUrl = app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=sendList',
        pag = this.data.pagel + 1,
        postData = {
          page: pag,
          token: tok
        }; 
    app.postLogin(postUrl, postData, this.initial); 
  },
  // 发出的
  initial: function (res) {
    if (res.data.code == 20000) {
      var datas = res.data;
      wx.hideLoading()
      this.setData({
        sendlist: datas.data.list,
        receiveAmount: datas.data.total_amount,
        receiveNum: datas.data.total_num,
        pagel: 0,
        lower0: true,
        sendHide: false,
        enevHide: true
      })

    }
  },
  // 收到的数据 加载
  loaddatar:function(){
    wx.showLoading({
      title: '加载中•••'
    })
    var tok = this.data.token;
    var postUrl = app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=sendEnveList',
        pag = this.data.pager + 1,
        postData = {
          page: pag,
          token: tok
        };
    app.postLogin(postUrl, postData, this.reciveList); 
  },
  //收到的包
  reciveList:function(res){
    if (res.data.code == 20000){
      var datas = res.data;
      wx.hideLoading();
      this.setData({
        pager: 0,
        sendAmount: datas.data.total_amount,
        sendNum: datas.data.total_num,
        enevlist: datas.data.list,
        lower1: true,
        sendHide: true,
        enevHide: false
      })
    }
  },
  //获取formid
  formSubmit: function (e) {
    var postData = {
        form_id: e.detail.formId,
        token: app.globalData.token
      };
    wx.request({
      url: app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=saveFormid',
      data: postData,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {       
      }
    })
  },
  //转发
  onShareAppMessage: function (res) {
    var id = res.target.id;
    var tct = this.data.sendlist[id];
    return {
      title: tct.remark,
      path: 'pages/give/give?tct_id=' + tct.tct_id,
      imageUrl: app.globalData.imgBaseUrl + 'sharefriend-' + tct.c_index+'.png'
    }
  }
})
