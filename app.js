//app.js
var loginInfo={};
App({
  setConfig: {url: 'https://www.qsune.com/tct'},
  //setConfig: { url: 'http://192.168.0.13/hb/admin' },
  globalData: {
    userInfo: null,
    token: '',
    news_key: '',
    wishes: [],
    moneys: [],
    feed_backs: [],
    imgBaseUrl: '',
    advUrl: '',
    helpList: [],
    balanceAdv: {},
    helpAdv: {},
    currentVersion: '1.0',
    checkStatus: false,
    tip_message: '',
    act_message:'',
    demoKoulin: [],
    rank_amount:''
  },

  onShow: function (options) {
    var that = this

    // wx.redirectTo({
    //   url: '/pages/give/give?tct_id=' + 374,
    // })

    wx.request({
      url: that.setConfig.url + '/index.php/User/login/config',
      data: {},
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: res => {
        if (res.data.data.check_version === that.globalData.currentVersion) {
          that.globalData.checkStatus = true;
          wx.redirectTo({
            url: '/pages/jieri/list',
          })
        }
      }
    })
    that.userLogin();
  },
  
  //登录
  userLogin: function(){
    var that = this;
    var codes;
    //获取登录code
    wx.login({
      success: function (res) {
        //console.log(res.code);
          loginInfo.code = res.code;
          codes = res.code;
          //获取用户信息
          wx.getSetting({
            success: res => {
              if (res.authSetting['scope.userInfo']) {
                // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                wx.getUserInfo({
                  success: res => {
                    // 可以将 res 发送给后台解码出 unionId
                    var infoUser = '';
                    that.globalData.userInfo = infoUser = res.userInfo;
                    // 所以此处加入 callback 以防止这种情况
                    if (that.userInfoReadyCallback) {
                      that.userInfoReadyCallback(res)
                    }
                    //用户信息入库
                    var url = that.setConfig.url + '/index.php/User/login/dologin';
                    var data = {
                      user_name: infoUser.nickName,
                      nick_name: infoUser.nickName,
                      head_img: infoUser.avatarUrl,
                      sex: infoUser.gender,
                      coutry: infoUser.country,
                      city: infoUser.city,
                      province: infoUser.province,
                      code: codes,
                    }                   
                    that.postLogin(url, data);
                  }
                })
              }else{
                wx.authorize({
                  scope: 'scope.userInfo',
                  success: res => {
                    //用户已经同意小程序授权
                    wx.getUserInfo({
                      success: res => {
                        // 可以将 res 发送给后台解码出 unionId
                        var infoUser = '';
                        that.globalData.userInfo = infoUser = res.userInfo;
                        // 所以此处加入 callback 以防止这种情况
                        if (that.userInfoReadyCallback) {
                          that.userInfoReadyCallback(res)
                        }
                        //用户信息入库
                        var url = that.setConfig.url + '/index.php/User/login/dologin';
                        var data = {
                          user_name: infoUser.nickName,
                          nick_name: infoUser.nickName,
                          head_img: infoUser.avatarUrl,
                          sex: infoUser.gender,
                          coutry: infoUser.country,
                          city: infoUser.city,
                          province: infoUser.province,
                          code: codes,
                        }
                        that.postLogin(url, data);
                      }
                    })
                  },
                  fail: res => {
                    wx.showModal({
                      title: '提示',
                      content: "小程序需要获取用户信息权限，点击确认前往设置或退出程序？",
                      showCancel: false,
                      success: res => {
                        if (res.confirm) {
                          wx.openSetting({
                            success: function (res) {
                              if (res.authSetting['scope.userInfo']) {
                                that.userLogin();
                              };
                            }
                          })
                        }
                      }
                    })
                  },
                  complete: function () { }
                })
              }
            }
          });
      }
    })
  },

  postLogin: function (url, data, callback = function () { }) {
    var that = this;
    wx.showLoading({
      title: '',
    })
    //发起网络请求
    wx.request({
      url: url,
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.code != 20000) {
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            mask: true,
            duration: 1500
          })
          if (res.data.code == 40500) { callback(res); }
          return false;
        }else{
          wx.hideLoading()
        }
        if (res.data.token) {
           that.globalData.token = res.data.token; 
           that.globalData.news_key = res.data.config.news_key;
           that.globalData.wishes = res.data.config.wishes;
           that.globalData.feed_backs = res.data.config.feed_backs;
           that.globalData.moneys = res.data.config.moneys;
           that.globalData.imgBaseUrl = res.data.config.img_base_url;
           that.globalData.helpList = res.data.config.help_list;
           that.globalData.balanceAdv = res.data.config.balance_adv;
           that.globalData.helpAdv = res.data.config.help_adv;
           that.globalData.tip_message = res.data.config.tip_message;
           that.globalData.act_message = res.data.config.act_message;
           that.globalData.demoKoulin = res.data.config.demoKoulin;
           that.globalData.rank_amount = res.data.config.rank_amount;
        }
        callback(res);
      }
    })
  }
})
